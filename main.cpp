#include <iostream>
#include <vector>
#include <fstream>
#include <list>
#include <algorithm>
#include <set>
#include <string>

using namespace std;

class Zadanie
{
public:
    Zadanie(size_t czas_, size_t id);
    Zadanie();
    size_t id,czas,es,ef,ls,lf;
};

Zadanie::Zadanie(size_t czas_, size_t id_)
{
    czas=czas_;
    id=id_;
}


class Projekt
{
    vector<Zadanie> zadania; // zadania w kolejnosci zgodnej z id
    vector<size_t> kolejnosc_topologiczna; // kolejnosc topologiczna (w formie id zadan)
    vector<size_t> kolejnosc_topologiczna_odwroconego; // kolejnosc topologiczna dla odwroconych kierukow
    vector< set<size_t> > krawedzie_do; //krawedzie wychodzace z kazdego wierzcholka
    vector< set<size_t> > krawedzie_z; //krawedze wchodzace do kazdego wierzcholka
    vector<size_t> sciezka_krytyczna;

    size_t dlugosc;

public:
    int wczytaj(string nazwa_pliku);

    void sortuj_topologicznie(); // wynik w zmiennej kolejnosc_topologiczna
    size_t dlugosc_projektu_topologicznie();
    void odwroc_sortuj_topologicznie(); //odwrocenie kierunkow krawedzi i posortowanie topologicznie, wynik w zmiennej kolejnosc_topologiczna_odwroconego
    void ls_lf(); //przejscie wstecz i oblicznie es oraz lf dla kazdego zadania

    void znajdz_sciezke_krytyczna();
    void odwiedz(size_t nr_wierzcholka, size_t last_finish, vector<size_t> sciezka);

    void wyswietl_zadania();
    void wyswietl_sciezke_krytyczna();

};

void Projekt::sortuj_topologicznie()
{
    list<Zadanie> zadania_lista(zadania.begin(),zadania.end()); //lista zeby szybciej usuwac elementy

    list<Zadanie>::iterator zadania_itr=zadania_lista.begin();

    vector< set<size_t> > krawedzie_z_tmp = krawedzie_z; //kopia, bo bedziemy usuwac krawedzie
    vector< set<size_t> > krawedzie_do_tmp = krawedzie_do;

    while(zadania_lista.size()>1)
    {
        while(!(krawedzie_do_tmp[(*zadania_itr).id].empty()) && zadania_itr!=(--zadania_lista.end())) //szukanie wierzcholka o stopniu wchodzacym 0..
            zadania_itr++;
        kolejnosc_topologiczna.push_back((*zadania_itr).id); //...dodawanie go do kolejnosci
        //usuwanie krawedzi zwiazanych z tym wierzcholkiem oraz jego samego
        for (auto &itr: krawedzie_z_tmp[(*zadania_itr).id])
        {
            krawedzie_do_tmp[itr].erase((*zadania_itr).id);
        }
        for (auto &itr: krawedzie_do_tmp[(*zadania_itr).id])
        {
            krawedzie_z_tmp[itr].erase((*zadania_itr).id);
        }
        krawedzie_z_tmp[(*zadania_itr).id].clear();
        krawedzie_do_tmp[(*zadania_itr).id].clear();

        zadania_lista.erase(zadania_itr);
        zadania_itr=zadania_lista.begin();
    }

    kolejnosc_topologiczna.push_back(zadania_lista.front().id); //ostatnie zadanie
}

void Projekt::odwroc_sortuj_topologicznie()
{
    list<Zadanie> zadania_lista(zadania.begin(),zadania.end()); //lista zeby szybciej usuwac elementy

    list<Zadanie>::iterator zadania_itr=zadania_lista.begin();

    vector< set<size_t> > krawedzie_z_tmp = krawedzie_do; //kopia, bo bedziemy usuwac krawedzie
    vector< set<size_t> > krawedzie_do_tmp = krawedzie_z;

    while(zadania_lista.size()>1)
    {
        while(!(krawedzie_do_tmp[(*zadania_itr).id].empty()) && zadania_itr!=(--zadania_lista.end())) //szukanie wierzcholka o stopniu wchodzacym 0..
            zadania_itr++;
        kolejnosc_topologiczna_odwroconego.push_back((*zadania_itr).id); //...dodawanie go do kolejnosci
        //usuwanie krawedzi zwiazanych z tym wierzcholkiem oraz jego samego
        for (auto &itr: krawedzie_z_tmp[(*zadania_itr).id])
        {
            krawedzie_do_tmp[itr].erase((*zadania_itr).id);
        }
        for (auto &itr: krawedzie_do_tmp[(*zadania_itr).id])
        {
            krawedzie_z_tmp[itr].erase((*zadania_itr).id);
        }
        krawedzie_z_tmp[(*zadania_itr).id].clear();
        krawedzie_do_tmp[(*zadania_itr).id].clear();

        zadania_lista.erase(zadania_itr);
        zadania_itr=zadania_lista.begin();
    }

    kolejnosc_topologiczna_odwroconego.push_back(zadania_lista.front().id); //ostatnie zadanie
}

size_t Projekt::dlugosc_projektu_topologicznie()
{
    size_t max_ef=0;
    for(auto &itr: kolejnosc_topologiczna)
    {
        size_t tmp_ef=0;
        for(auto &itr2: krawedzie_do[itr])
        {
            tmp_ef=max(tmp_ef, zadania[itr2].ef);
        }
        zadania[itr].es=tmp_ef;
        zadania[itr].ef=tmp_ef+zadania[itr].czas;
        max_ef=max(max_ef,zadania[itr].ef);
    }
    dlugosc=max_ef;
    return max_ef;
}

void Projekt::ls_lf()
{
    zadania[kolejnosc_topologiczna_odwroconego[0]].lf=dlugosc;
    zadania[kolejnosc_topologiczna_odwroconego[0]].ls=zadania[kolejnosc_topologiczna_odwroconego[0]].lf-zadania[kolejnosc_topologiczna_odwroconego[0]].czas;
    for(auto &itr: kolejnosc_topologiczna_odwroconego)
    {
        size_t tmp_ls=dlugosc;
        for(auto &itr2: krawedzie_z[itr])
        {
            tmp_ls=min(tmp_ls, zadania[itr2].ls);
        }
        zadania[itr].lf=tmp_ls;
        zadania[itr].ls=tmp_ls-zadania[itr].czas;
    }
}


//nr_wierzcholka - odwiedzany wierzcholek
//last finish - koniec poprzedniego zadania (wierzcholka)
//sciazka - zapamietuje przechodzona sciezke
void Projekt::odwiedz(size_t nr_wierzcholka, size_t last_finish, vector<size_t> sciezka)
{
    if (zadania[nr_wierzcholka].lf==zadania[nr_wierzcholka].ef && last_finish==zadania[nr_wierzcholka].es) //zadanie bez poslizgu
    {
        sciezka.push_back(nr_wierzcholka);
        if (zadania[nr_wierzcholka].ef==dlugosc) //jezeli jest koncem
        {
            sciezka_krytyczna=sciezka;
        }
        else
        {
            for(auto & itr: krawedzie_z[nr_wierzcholka])
            {
                odwiedz(itr, zadania[nr_wierzcholka].ef, sciezka);
            }
        }
    }
}

void Projekt::znajdz_sciezke_krytyczna()
{
    for (auto & itr: kolejnosc_topologiczna)
    {
        if (krawedzie_do[itr].empty())
            odwiedz(itr,0,vector<size_t>());
    }
}

void Projekt::wyswietl_zadania()
{
    for(auto &itr: zadania)
    {
        cout << itr.es << " " << itr.ef << " " << itr.ls << " " << itr.lf << endl;
    }
}

void Projekt::wyswietl_sciezke_krytyczna()
{
    for(auto & itr: sciezka_krytyczna)
        cout << itr+1 << " " << zadania[itr].es << " " << zadania[itr].ef << endl;
}

int Projekt::wczytaj(string nazwa_pliku)
{
    ifstream plik;
    size_t i;
    size_t liczba_zadan=0;
    size_t liczba_polaczen=0;

    plik.open(nazwa_pliku.c_str(),ios::out);
    if (!(plik.is_open()))
    {
        cout << "Blad otwierania pliku";
        return 0;
    }
    else
    {
        plik >> liczba_zadan;
        plik >> liczba_polaczen;
        for(i=0;i<liczba_zadan;i++)
        {
            size_t czas_tmp;
            plik >> czas_tmp;
            zadania.push_back(Zadanie(czas_tmp,i));
            krawedzie_do.push_back(set<size_t>());
            krawedzie_z.push_back(set<size_t>());

        }
        for(i=0;i<liczba_polaczen;i++)
        {
            size_t krawedz_z, krawedz_do;
            plik >> krawedz_z;
            plik >> krawedz_do;
            krawedzie_do[krawedz_do-1].insert(krawedz_z-1);
            krawedzie_z[krawedz_z-1].insert(krawedz_do-1);
        }
    }
    plik.close();
    return 1;
}


int main()
{
    Projekt projekt;
    if (projekt.wczytaj("data70.txt"))
    {
        projekt.sortuj_topologicznie();
        cout << "process time:" << endl << projekt.dlugosc_projektu_topologicznie() << endl;
        projekt.odwroc_sortuj_topologicznie();
        projekt.ls_lf();
        cout << "earlyStart earlyFinish lateStart lateFinish:" << endl;
        projekt.wyswietl_zadania();
        cout << "critical path:" << endl;
        projekt.znajdz_sciezke_krytyczna();
        projekt.wyswietl_sciezke_krytyczna();
    }
    return 0;
}

