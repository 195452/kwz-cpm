TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp

include(deployment.pri)
qtcAddDeployment()

DISTFILES += \
    data10.txt \
    data20.txt \
    data30.txt \
    data40.txt \
    data50.txt \
    data60.txt \
    data70.txt \
    data80.txt

